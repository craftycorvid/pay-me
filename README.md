# Pay Me

Make sense of a tech-flavored hourly rate. See it in action:
_[Pay Me](https://craftycorvid.gitlab.io/pay-me/)_ on GitLab Pages.

As a contractor, what goes into figuring your hourly rate? It might not be as
straightforward as you think!

_Pay Me_ applies reasonable defaults or customized parameters to:

  * target annual salary
  * benefits
  * vacation
  * hours per week

## Development

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

## Roadmap

- [ ] MVP: offline salary calculation
- [ ] Persistence: salary submission, privacy policy
- [ ] Outreach: branding, alpha, invite folks to try it out
- [ ] Maintenance: find project maintainer, get feedback, make enhancements, fix bugs
